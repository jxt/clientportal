<!DOCTYPE html>
<html>
<head lang="en">
    <link rel="stylesheet" type="text/css" href="css/main.less"/>
    <meta charset="UTF-8">
    <title>JXT Client Portal</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/html5shiv.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/fileinput.min.js"></script>

    <script src="js/jquery.ui.widget.js"></script>
    <script src="js/jquery.iframe-transport.js"></script>
    <script src="js/jquery.fileupload.js"></script>

    <link rel="stylesheet" href="css/normalize.css"/>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/datepicker3.css">
    <link rel="stylesheet" href="css/fileinput.min.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,700italic,900' rel='stylesheet'
          type='text/css'>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet'
          type='text/css'>
    <link rel="stylesheet" href="css/jquery.fileupload.css">
    <link rel="stylesheet" href="css/main.css"/>

</head>
<body>

<div class="container">
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 logo-holder">
                    <img src="img/logo-portal.svg" alt="JXT Portal Logo" class="img-responsive"/>
                </div>
                <div class="col-sm-3">
                    <div class="username-display">
                        <h1>Welcome</h1>

                        <h3>Neil Kearney</h3>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="pull-right login-status">
                        <a href="http://www.jxt.com.au/admin">
                            <button class="btn btn-default" type="button"><span
                                    class="glyphicon glyphicon-log-in"></span>&nbsp; Login
                            </button>
                        </a>
                        <!--when logged in as user display following-->

                        <!--<a href="#">-->
                        <!--<button class="btn btn-default" type="button"><span class="glyphicon glyphicon-log-out"></span> Logout</button>-->
                        <!--</a>-->
                    </div>
                </div>
            </div>
        </div>

    </header>