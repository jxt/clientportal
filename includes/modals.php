<!-- Modal for add task -->
<div class="modal fade" id="addTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="addtasklabel">Add a task</h2>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="form-group"><input type="text" class="form-control" placeholder="Task name"></div>
                    <div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"
                                                      placeholder="Task description"></textarea></div>
                    <div class="form-group">
                        <div class="input-daterange input-group" id="datepicker">
                            <input type="text" class="form-control" name="start" placeholder="From"/>
                            <span class="input-group-addon">to</span>
                            <input type="text" class="form-control" name="end" placeholder="To"/>
                        </div>
                    </div>

                    <input id="TaskFIleUpload" type="file" multiple="true">

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add Task</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal for add task -->
<div class="modal fade" id="approveTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="addtasklabel">Approve task</h2>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="form-group"><input type="text" class="form-control" placeholder="Task name"></div>
                    <div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"
                                                      placeholder="Task description"></textarea></div>
                    <div class="form-group">
                        <h4>Approve task</h4>

                        <div class="btn-group btn-toggle">
                            <button class="btn btn-default">Yes</button>
                            <button class="btn btn-primary active">No</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Confirm</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<!-- Modal for delete task -->
<div class="modal fade" id="deleteTask" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="addtasklabel">Delete task</h2>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this task?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"><i class="fa fa-trash-o"></i> Yes, delete</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>

<!-- Modal for add comment -->
<div class="modal fade" id="addComment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="myModalLabel">Add a Comment</h2>
            </div>
            <div class="modal-body">
                <form action="">

                    <div class="form-group"><textarea name="" id="" cols="30" rows="5" class="form-control"
                                                      placeholder="Enter comment"></textarea></div>


                    <!-- file upload -->
                    <input id="CommentFileUpload" type="file" multiple="true">

                    <br>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"><i class="fa fa-comment"></i> Add Comment</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<!-- Modal for SEO Description -->
<div class="modal fade" id="SeoDescription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="addtasklabel">Meta Description</h2>
            </div>
            <div class="modal-body">
                <p>The meta description should employ the keywords intelligently, but also create a compelling
                    description that
                    a searcher will want to click. The short paragraph gives opportunity to advertise content to
                    searchers and let
                    them know exactly what the given page has with regard to what they're looking for.</p>

                <h3>Benefit</h3>

                <p>The meta description tag serves the function of advertising copy, drawing readers to a website from
                    the
                    results and thus, is an extremely important part of search marketing.</p>

                <h3>Maximum length</h3>

                <p>156 Characters</p>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>

<!-- Modal for SEO Description -->
<div class="modal fade" id="SeoKeywords" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="addtasklabel">Meta Keywords</h2>
            </div>
            <div class="modal-body">
                <p>The keywords tag is used by the meta capable search engines to help aid them in indexing your
                    website. This
                    is important; the search engines use this information to determine under what queries your website
                    will come
                    up under.</p>

                <h3>Benefit</h3>

                <p>What are the keywords associated with your business, i.e. what roles do you recruit for? This enables
                    you to
                    be specific in terms of job specific roles and industry.</p>

                <h3>Maximum length</h3>

                <p>255 characters.</p>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>

            </div>
        </div>
    </div>
</div>