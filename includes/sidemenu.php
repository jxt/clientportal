<div class="col-sm-2 sidebar-section">
    <ul>
        <a href="order1.php" id="orderTab">
            <li>
                <h1>My Order</h1>

                <h3>view your details</h3>
            </li>
        </a>
        <a href="#">
            <li>
                <h1>My Design</h1>

                <h3>design questionnaire</h3>
            </li>
        </a>
        <a href="#" class="active" id="salesTab">
            <li>
                <h1>Sales Handover</h1>

                <h3>please approve</h3>
            </li>
        </a>
        <a href="index.php" id="projectTab">
            <li>
                <h1>My Project</h1>

                <h3>manage your project</h3>
            </li>
        </a>
        <a href="sitemap.php" id="sitemapTab">
            <li>
                <h1>My Sitemap</h1>

                <h3>create your sitemap</h3>
            </li>
        </a>

        <a href="#">
            <li>
                <h1>My Social media</h1>

                <h3>our questionnaire</h3>
            </li>
        </a>
    </ul>

</div>