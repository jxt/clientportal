<?php include 'includes/header.php'; ?>

<div class="container-fluid">
    <div class="row height-ref">

        <?php include 'includes/sidemenu.php'; ?>

        <div class="col-sm-10 portal-section">
            <div class="row">
                <div class="col-sm-8">
                    <ul class="project-status">
                        <li>24 <span class="projectStatusSubline">days in</span></li>
                        <li>65% <span class="projectStatusSubline">Complete</span></li>
                        <li>2 <span class="projectStatusSubline">tasks pending</span></li>
                    </ul>
                </div>
                <div class="col-sm-4 text-right">
                    <a href="#" class="add-task-btn">
                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#addTask"><i
                                class="fa fa-plus-circle"></i> Add a task
                        </button>
                    </a>
                </div>
            </div>
            <br/>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                               aria-controls="collapseOne" class="accordion-toggle collapsed">
                                <div class="col-sm-6">Design Questionnaire</div>

                                <div class="col-sm-5">
                                    <span class="taskStatus pull-right text-right">In progress <span
                                            class="glyphicon glyphicon-transfer"></span>
</span>
                                    <span class="dateOfTask pull-right text-right">12/12/2014 to 29/10/14</span>
                                </div>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h2>Description</h2>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto at
                                distinctio dolores, excepturi exercitationem harum inventore ipsum laboriosam minima
                                minus odit porro possimus qui reprehenderit veniam voluptas. Sapiente, similique?</p>

                            <h2>Files</h2>
                            <ul class="files-task">
                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design1.jpg</a>
                                </li>
                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design2.jpg</a>
                                </li>
                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design3.jpg</a>
                                </li>

                            </ul>
                            <div class="row">
                                <div class="col-sm-12 text-right">

                                    <a href="#">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#addComment"><i
                                                class="fa fa-comments"></i> Add Comment
                                        </button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#approveTask">
                                            <i class="fa fa-comments"></i> Approve task
                                        </button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-primary edit-task-btn" data-toggle="modal"
                                                data-target="#addTask"><i class="fa fa-pencil-square-o"></i> Edit Task
                                        </button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#deleteTask"><i
                                                class="fa fa-trash-o"></i> Delete Task
                                        </button>
                                    </a></div>
                            </div>
                            <hr/>

                            <div class="panel panel-default chat-cell-left col-sm-10 pull-left">
                                <div class="panel-body">
                                    <strong class="author"><i class="fa fa-comments"></i> Jack Sparrow</strong>
                                    <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span> 5:20pm | 12/12/2014
                                    </small>
                                    <p id="comment1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet
                                        delectus dolores ea facilis hic libero magni molestiae rerum sed sit. Amet aut
                                        blanditiis culpa ea fugiat officia quia tempora vel?</p>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <h2>Files</h2>
                                            <ul class="files-task">
                                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design1.jpg</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4"><a href="#" class="commentReply">
                                                <button class="btn btn-success replyToComment" data-toggle="modal"
                                                        data-target="#addComment">Reply <i class="fa fa-reply"></i>
                                                </button>
                                            </a></div>
                                    </div>


                                </div>
                            </div>

                            <div class="panel panel-default chat-cell-right col-sm-10 pull-right">
                                <div class="panel-body">
                                    <strong class="author"><i class="fa fa-comments"></i> Jack Sparrow</strong>
                                    <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span> 5:24pm | 12/12/2014
                                    </small>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet delectus dolores
                                        ea facilis hic libero magni molestiae rerum sed sit. Amet aut blanditiis culpa
                                        ea fugiat officia quia tempora vel?</p>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <h2>Files</h2>
                                            <ul class="files-task">
                                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design1.jpg</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4"><a href="#" class="commentReply">
                                                <button class="btn btn-success replyToComment" data-toggle="modal"
                                                        data-target="#addComment">Reply <i class="fa fa-reply"></i>
                                                </button>
                                            </a></div>
                                    </div>


                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true"
                               aria-controls="collapseOne" class="accordion-toggle collapsed">
                                <div class="col-sm-6">Upload content for population</div>

                                <div class="col-sm-5">
                                    <span class="taskStatus pull-right text-right">Completed <span
                                            class="glyphicon glyphicon-ok-circle"></span>
</span>
                                    <span class="dateOfTask pull-right text-right">12/12/2014 to 29/10/14</span>
                                </div>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h2>Description</h2>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto at
                                distinctio dolores, excepturi exercitationem harum inventore ipsum laboriosam minima
                                minus odit porro possimus qui reprehenderit veniam voluptas. Sapiente, similique?</p>

                            <h2>Files</h2>
                            <ul class="files-task">
                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design1.jpg</a>
                                </li>
                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design2.jpg</a>
                                </li>
                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design3.jpg</a>
                                </li>

                            </ul>
                            <div class="row">
                                <div class="col-sm-12 text-right">

                                    <a href="#">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#addComment"><i
                                                class="fa fa-comments"></i> Add Comment
                                        </button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-primary edit-task-btn" data-toggle="modal"
                                                data-target="#addTask"><i class="fa fa-pencil-square-o"></i> Edit Task
                                        </button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#deleteTask"><i
                                                class="fa fa-trash-o"></i> Delete Task
                                        </button>
                                    </a></div>
                            </div>
                            <hr/>

                            <div class="panel panel-default chat-cell-left col-sm-10 pull-left">
                                <div class="panel-body">
                                    <strong class="author"><i class="fa fa-comments"></i> Jack Sparrow</strong>
                                    <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span> 5:20pm | 12/12/2014
                                    </small>
                                    <p id="comment1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet
                                        delectus dolores ea facilis hic libero magni molestiae rerum sed sit. Amet aut
                                        blanditiis culpa ea fugiat officia quia tempora vel?</p>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <h2>Files</h2>
                                            <ul class="files-task">
                                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design1.jpg</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4"><a href="#" class="commentReply">
                                                <button class="btn btn-success replyToComment" data-toggle="modal"
                                                        data-target="#addComment">Reply <i class="fa fa-reply"></i>
                                                </button>
                                            </a></div>
                                    </div>


                                </div>
                            </div>

                            <div class="panel panel-default chat-cell-right col-sm-10 pull-right">
                                <div class="panel-body">
                                    <strong class="author"><i class="fa fa-comments"></i> Jack Sparrow</strong>
                                    <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span> 5:24pm | 12/12/2014
                                    </small>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet delectus dolores
                                        ea facilis hic libero magni molestiae rerum sed sit. Amet aut blanditiis culpa
                                        ea fugiat officia quia tempora vel?</p>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <h2>Files</h2>
                                            <ul class="files-task">
                                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design1.jpg</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4"><a href="#" class="commentReply">
                                                <button class="btn btn-success replyToComment" data-toggle="modal"
                                                        data-target="#addComment">Reply <i class="fa fa-reply"></i>
                                                </button>
                                            </a></div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                               aria-expanded="true" aria-controls="collapseOne" class="accordion-toggle collapsed">
                                <div class="col-sm-6">Complete sitemap</div>

                                <div class="col-sm-5">
                                    <span class="taskStatus pull-right text-right">Approved <span
                                            class="glyphicon glyphicon-ok-sign"></span>
</span>
                                    <span class="dateOfTask pull-right text-right">12/12/2014 to 29/10/14</span>
                                </div>
                            </a>
                        </h4>
                        <div class="clearfix"></div>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel"
                         aria-labelledby="headingOne">
                        <div class="panel-body">
                            <h2>Description</h2>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto at
                                distinctio dolores, excepturi exercitationem harum inventore ipsum laboriosam minima
                                minus odit porro possimus qui reprehenderit veniam voluptas. Sapiente, similique?</p>

                            <h2>Files</h2>
                            <ul class="files-task">
                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design1.jpg</a>
                                </li>
                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design2.jpg</a>
                                </li>
                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design3.jpg</a>
                                </li>

                            </ul>
                            <div class="row">
                                <div class="col-sm-12 text-right">

                                    <a href="#">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#addComment"><i
                                                class="fa fa-comments"></i> Add Comment
                                        </button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-primary edit-task-btn" data-toggle="modal"
                                                data-target="#addTask"><i class="fa fa-pencil-square-o"></i> Edit Task
                                        </button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#deleteTask"><i
                                                class="fa fa-trash-o"></i> Delete Task
                                        </button>
                                    </a></div>
                            </div>
                            <hr/>

                            <div class="panel panel-default chat-cell-left col-sm-10 pull-left">
                                <div class="panel-body">
                                    <strong class="author"><i class="fa fa-comments"></i> Jack Sparrow</strong>
                                    <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span> 5:20pm | 12/12/2014
                                    </small>
                                    <p id="comment1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet
                                        delectus dolores ea facilis hic libero magni molestiae rerum sed sit. Amet aut
                                        blanditiis culpa ea fugiat officia quia tempora vel?</p>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <h2>Files</h2>
                                            <ul class="files-task">
                                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design1.jpg</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4"><a href="#" class="commentReply">
                                                <button class="btn btn-success replyToComment" data-toggle="modal"
                                                        data-target="#addComment">Reply <i class="fa fa-reply"></i>
                                                </button>
                                            </a></div>
                                    </div>


                                </div>
                            </div>

                            <div class="panel panel-default chat-cell-right col-sm-10 pull-right">
                                <div class="panel-body">
                                    <strong class="author"><i class="fa fa-comments"></i> Jack Sparrow</strong>
                                    <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span> 5:24pm | 12/12/2014
                                    </small>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet delectus dolores
                                        ea facilis hic libero magni molestiae rerum sed sit. Amet aut blanditiis culpa
                                        ea fugiat officia quia tempora vel?</p>

                                    <div class="row">
                                        <div class="col-sm-8">
                                            <h2>Files</h2>
                                            <ul class="files-task">
                                                <li><span class="glyphicon glyphicon-picture"></span> <a href="">Hompage_design1.jpg</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-4"><a href="#" class="commentReply">
                                                <button class="btn btn-success replyToComment" data-toggle="modal"
                                                        data-target="#addComment">Reply <i class="fa fa-reply"></i>
                                                </button>
                                            </a></div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/modals.php'; ?>

<?php include 'includes/footer.php'; ?>


</body>
</html>