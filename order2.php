<?php include 'includes/header.php'; ?>

<div class="container-fluid">
    <div class="row height-ref">

        <?php include 'includes/sidemenu.php'; ?>

        <div class="col-sm-10 order-section">
            <div class="row">
                <div class="col-sm-12">
                    <h1><span class="title-sub">Order No: </span> #JXTRef001942 <span class="title-sub">Company:</span>
                        JXT <span class="title-sub">BDM:</span> Neil Kearney </h1>

                    <p>Please follow the simple steps below to complete your order.</p>

                    <ul class="nav nav-pills nav-wizard">
                        <li class="active"><a href="order1.php">1. Your existing order</a>

                            <div class="nav-arrow"></div>
                        </li>
                        <li>
                            <div class="nav-wedge"></div>
                            <a href="order2.php">2. More Products from JXT</a>

                            <div class="nav-arrow"></div>
                        </li>
                        <li>
                            <div class="nav-wedge"></div>
                            <a href="order3.php">3. Confirmation of Order</a></li>
                    </ul>

                    <p>JXT offer also the following products. If you would like a JXT BDM to contact you to discuss any
                        of these products please check 'I am interested'</p>
                    <br>

                    <div class="products-section row">

                        <div class="col-sm-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><img src="img/recruiter-directory.jpg" alt=""
                                                                class="img-responsive"></div>
                                <div class="panel-body text-center">
                                    <h3>Name of Product</h3>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="">
                                            I am interested in this product
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><img src="img/mintii.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="panel-body text-center">
                                    <h3>Name of Product</h3>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="">
                                            I am interested in this product
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><img src="img/maestro.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="panel-body text-center">
                                    <h3>Name of Product</h3>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="">
                                            I am interested in this product
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><img src="img/salary-spy.jpg" alt="" class="img-responsive">
                                </div>
                                <div class="panel-body text-center">
                                    <h3>Name of Product</h3>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="">
                                            I am interested in this product
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><img src="img/recruiter-directory.jpg" alt=""
                                                                class="img-responsive"></div>
                                <div class="panel-body text-center">
                                    <h3>Name of Product</h3>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="">
                                            I am interested in this product
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="panel panel-default">
                                <div class="panel-heading"><img src="img/recruiter-directory.jpg" alt=""
                                                                class="img-responsive"></div>
                                <div class="panel-body text-center">
                                    <h3>Name of Product</h3>

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="">
                                            I am interested in this product
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <a href="order3.php">
                        <button type="button" class="btn btn-success pull-right">Next <span
                                class="glyphicon glyphicon-chevron-right"></span></button>
                    </a>
                    <a href="order1.php">
                        <button type="button" class="btn btn-success pull-left"><span
                                class="glyphicon glyphicon-chevron-left"></span> Previous
                        </button>
                    </a>


                </div>
            </div>
            <br/>


        </div>
    </div>
</div>
</div>

<?php include 'includes/modals.php'; ?>

<?php include 'includes/footer.php'; ?>




</body>
</html>