<?php include 'includes/header.php'; ?>

<div class="container-fluid">
    <div class="row height-ref">

        <?php include 'includes/sidemenu.php'; ?>

        <div class="col-sm-10 order-section">
            <div class="row">
                <div class="col-sm-12">
                    <h1><span class="title-sub">Order No: </span> #JXTRef001942 <span class="title-sub">Company:</span>
                        JXT <span class="title-sub">BDM:</span> Neil Kearney </h1>

                    <p>Please follow the simple steps below to complete your order.</p>

                    <ul class="nav nav-pills nav-wizard">
                        <li><a href="order1.php">1. Your existing order</a>

                            <div class="nav-arrow"></div>
                        </li>
                        <li>
                            <div class="nav-wedge"></div>
                            <a href="order2.php">2. More Products from JXT</a>

                            <div class="nav-arrow"></div>
                        </li>
                        <li class="active">
                            <div class="nav-wedge"></div>
                            <a href="order3.php">3. Confirmation of Order</a></li>
                    </ul>


                    <div class="checkout-section">

                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                </tr>

                                </thead>
                                <tbody>
                                <tr>
                                    <td>Custom Responsive Recruitment Website</td>
                                    <td>1</td>
                                    <td>15,000</td>
                                </tr>
                                <tr>
                                    <td>Custom Responsive Recruitment Website</td>
                                    <td>1</td>
                                    <td>15,000</td>
                                </tr>
                                <tr>
                                    <td>Custom Responsive Recruitment Website</td>
                                    <td>1</td>
                                    <td>15,000</td>
                                </tr>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="2"><strong>Total:</strong></td>
                                    <td><span class="currencySymbol">$</span>45,000</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>GST:</strong></td>
                                    <td><span class="currencySymbol">$</span>4,500</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>Net Total:</strong></td>
                                    <td><span class="currencySymbol">$</span>49,500</td>
                                </tr>
                                </tfoot>
                            </table>

                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Monthly Licence</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                </tr>

                                </thead>
                                <tbody>
                                <tr>
                                    <td>Boardy - Monthly SAAS Licensing Fees</td>
                                    <td>1</td>
                                    <td>500</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="2"><strong>Total:</strong></td>
                                    <td><span class="currencySymbol">$</span>500</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>GST:</strong></td>
                                    <td><span class="currencySymbol">$</span>50</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><strong>Monthly Fee:</strong></td>
                                    <td><span class="currencySymbol">$</span>550</td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="panel panel-default col-sm-6 authorised-person">
                            <div class="panel-body">
                                <input type="text" class="form-control" placeholder="Please enter the name of the authorised person
">
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <a href="#">
                        <button type="button" class="btn btn-success pull-right" style="margin-left:10px;"><span
                                class="glyphicon glyphicon-ok"></span> Authenticate &amp; Confirm
                        </button>
                    </a>
                    <a href="#">
                        <button type="button" class="btn btn-info pull-right"><span
                                class="glyphicon glyphicon-print"></span> Print your order
                        </button>
                    </a>

                    <a href="order2.php">
                        <button type="button" class="btn btn-success pull-left"><span
                                class="glyphicon glyphicon-chevron-left"></span> Previous
                        </button>
                    </a>


                </div>
            </div>
            <br/>


        </div>
    </div>
</div>
</div>

<?php include 'includes/modals.php'; ?>

<?php include 'includes/footer.php'; ?>




</body>
</html>