/**
 * Created by neil on 22/12/2014.
 */
$("#TaskFIleUpload").fileinput({
    browseClass: "btn btn-info btn-block",
    showCaption: false,
    showRemove: false,
    showUpload: false
});
$("#CommentFileUpload").fileinput({
    browseClass: "btn btn-info btn-block",
    showCaption: false,
    showRemove: false,
    showUpload: false
});

$('.collapse').collapse();
$('.input-daterange').datepicker({
    autoclose: true,
    format: "dd/mm/yyyy",
    todayHighlight: true
});

$('.btn-toggle').click(function () {
    $(this).find('.btn').toggleClass('active');

    if ($(this).find('.btn-primary').size() > 0) {
        $(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').size() > 0) {
        $(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').size() > 0) {
        $(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').size() > 0) {
        $(this).find('.btn').toggleClass('btn-info');
    }

    $(this).find('.btn').toggleClass('btn-default');

});

// $('form').submit(function(){
// 	alert($(this["options"]).val());
//     return false;
// });

$('.edit-task-btn').click(function () {
    $('#addtasklabel').text('Edit task');
});

$('.add-task-btn').click(function () {
    $('#addtasklabel').text('Add task');
});

$('.replyToComment').click(function () {
    var authorReplyComment = $("#comment1").html();
    return authorReplyComment;
});

$(function () {
    if (document.location.href.indexOf('order') > -1) {
        $("#orderTab").addClass("active");
        $("#salesTab").removeClass("active");
    }
});

$(function () {
    if (document.location.href.indexOf('index') > -1) {
        $("#projectTab").addClass("active");
        $("#salesTab").removeClass("active");
    }
});

$(function () {
    if (document.location.href.indexOf('sitemap') > -1) {
        $("#sitemapTab").addClass("active");
        $("#salesTab").removeClass("active");
    }
});


$(".product-detail").hide();
$("button.showDetails").click(function () {
    $(".product-detail").slideToggle("slow");
    $(this).text(function (i, text) {
        return text === "View Details" ? "Hide Details" : "View Details";
    })
});


$(function () {
    $(window).resize(function () {
        $('.sidebar-section').height($('.height-ref').height() - 20);
    });
    $(window).resize();
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})


