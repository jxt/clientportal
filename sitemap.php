<?php include 'includes/header.php'; ?>

<div class="container-fluid">
    <div class="row height-ref">

        <?php include 'includes/sidemenu.php'; ?>

        <div class="col-sm-10 sitemap-section">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Create your sitemap below</h1>


                    <h2>Generic Email Addresses <i class="fa fa-question-circle" data-toggle="tooltip"
                                                   data-placement="right"
                                                   title="The following emails are used for all system notifications of your new SaaS solution"></i>
                    </h2>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Submit Resume</label>
                                <input class="form-control" type="text" placeholder="enter email address"/>
                            </div>
                            <div class="form-group">
                                <label for="">Submit Job Vacancy</label>
                                <input class="form-control" type="text" placeholder="enter email address"/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Generic Contact us</label>
                                <input class="form-control" type="text" placeholder="enter email address"/>
                            </div>
                            <div class="form-group">
                                <label for="">Job applications</label>
                                <input class="form-control" type="text" placeholder="enter email address"/>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <a href="">
                                <button class="btn btn-success pull-right">Save email addresses &nbsp;<i
                                        class="fa fa-download"></i></button>
                            </a>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-7">
                            <h2>My Pages <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                                            title="Add pages to your sitemap by clicking New Page"></i></h2>

                            <p>Create new pages then drag and drop them.</p>
                            <a href="">
                                <button class="btn btn-success pull-right newPageButton">New page <i
                                        class="fa fa-plus"></i></button>
                            </a>
                            <hr/>
                        </div>
                        <div class="col-sm-5">
                            <h2>Page Properties</h2>

                            <p>Click a page item to view it's properties.</p>
                            <hr/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-7" id="sitemapHolder">
                            <ul id="map-items" class="mi ui-sortable">
                                <li id="map-item-#ID" class="map-item clear">
                                    <div class="primaryPage">
                                        <span class="page-title">Home</span>
                                        <a href=""><span class="map-item-remove pull-right"><i
                                                    class="fa fa-remove"></i></span></a>
                                        <span class="files-uploaded pull-right"><i class="fa fa-file-text-o"></i></span>
                                    </div>
                                </li>

                                <li id="map-item-#ID" class="map-item clear">
                                    <div class="primaryPage">
                                        <span class="page-title">About Us</span>
                                        <a href=""><span class="map-item-remove pull-right"><i
                                                    class="fa fa-remove"></i></span></a>
                                        <span class="files-uploaded pull-right"><i class="fa fa-file-text-o"></i></span>
                                    </div>
                                    <ul>
                                        <li>
                                            <div class="subPage">
                                                <span class="page-title">About Us</span>
                                                <a href=""><span class="map-item-remove pull-right"><i
                                                            class="fa fa-remove"></i></span></a>
                                                <span class="files-uploaded pull-right"><i
                                                        class="fa fa-file-text-o"></i></span>
                                            </div>
                                            <ul>
                                                <li>
                                                    <div class="subPage">
                                                        <span class="page-title">About Us</span>
                                                        <a href=""><span class="map-item-remove pull-right"><i
                                                                    class="fa fa-remove"></i></span></a>
                                                        <span class="files-uploaded pull-right"><i
                                                                class="fa fa-file-text-o"></i></span>
                                                    </div>
                                                </li>
                                            </ul>

                                        </li>
                                    </ul>
                                </li>
                                <li id="map-item-#ID" class="map-item clear">
                                    <div class="primaryPage">
                                        <span class="page-title">Meet the team</span>
                                        <a href=""><span class="map-item-remove pull-right"><i
                                                    class="fa fa-remove"></i></span></a>
                                        <span class="files-uploaded pull-right"><i class="fa fa-file-text-o"></i></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-5" id="propertyHolder">
                            <div class="form-group">
                                <label>Name <span class="required">*</span></label>
                                <input class="form-control" type="text" placeholder=""/>
                            </div>
                            <div class="form-group">
                                <label>URL</label>
                                <input class="form-control" type="text" placeholder=""/>
                            </div>
                            <div class="form-group">
                                <label>Title <span class="required">*</span></label>
                                <input class="form-control" type="text" placeholder=""/>
                            </div>

                            <div class="form-group">
                                <label>Meta Description <a data-toggle="modal" href='#SeoDescription'><i
                                            class="fa fa-question-circle"></i></a></label>
                                <textarea class="form-control" placeholder="" cols="30" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords <a data-toggle="modal" href='#SeoKeywords'><i
                                            class="fa fa-question-circle"></i></a></label>
                                <textarea class="form-control" placeholder="" cols="30" rows="3"></textarea>
                            </div>
                            <div class="form-group attachments">
                                <label>Attachment(s)</label>

                                <div class="row">
                                    <div class="col-sm-12 files" id="files">
                                        <!-- The container for the uploaded files -->

                                        <a href="">Boardy private site</a>

                                        <span class="actions pull-right">
                                            <a href="#"><i class="fa fa-remove"></i></a>
                                            <a href="#"><i class="fa fa-upload"></i></a>
                                        </span>
                                        <span class="file-type pull-right">docx</span>
                                    </div>
                                    <br>
                                    <br>

                                    <div class="col-sm-12">
                                         <span class="btn btn-info fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <span>Select files...</span>
                                            <!-- The file input field used as target for the file upload widget -->
                                            <input id="fileupload" type="file" name="files[]" multiple>
                                        </span>
                                        <br>
                                        <br>
                                        <!-- The global progress bar -->
                                        <div id="progress" class="progress">
                                            <div class="progress-bar progress-bar-success"></div>
                                        </div>

                                    </div>
                                    <div class="col-sm-12">
                                        <a href="">
                                            <button class="btn btn-success pull-right">Save page properties &nbsp;<i
                                                    class="fa fa-download"></i></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="total-pages">Total Pages: 5/25</div>
                                <a class="finalise-btn pull-right" href="">Finalise</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <br/>


    </div>
</div>
</div>
</div>

<?php include 'includes/modals.php'; ?>

<?php include 'includes/footer.php'; ?>


</body>
</html>