<?php include 'includes/header.php'; ?>

<div class="container-fluid">
    <div class="row height-ref">

        <?php include 'includes/sidemenu.php'; ?>

        <div class="col-sm-10 dashboard-section">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Dashboard</h1>

   <div class="row">
       <div class="col-sm-6"><h2>Your Orders</h2></div>
       <div class="col-sm-6"><span class="activeProject"><span class="activeProjectGreen">Active Project:</span> Responsive Job Board</span> </div>
   </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive yourOrders">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Order No.</th>
                                        <th>Client</th>
                                        <th>Company</th>
                                        <th>BDM</th>
                                        <th>Purchased items</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row"><a href="#">JXT1231212</a></th>
                                        <td>Natalie Harper</td>
                                        <td>Chandler Macleod</td>
                                        <td>Rick Mare</td>
                                        <td>Responsive Jobboard <br/>
                                            Professional Services<br/>
                                            Pintros Standard Package</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><a href="#">JXT1231212</a></th>
                                        <td>Natalie Harper</td>
                                        <td>Chandler Macleod</td>
                                        <td>Rick Mare</td>
                                        <td>Responsive Jobboard</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><a href="#">JXT1231212</a></th>
                                        <td>Natalie Harper</td>
                                        <td>Chandler Macleod</td>
                                        <td>Rick Mare</td>
                                        <td>Responsive Jobboard</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><a href="#">JXT1231212</a></th>
                                        <td>Natalie Harper</td>
                                        <td>Chandler Macleod</td>
                                        <td>Rick Mare</td>
                                        <td>Responsive Jobboard</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><a href="#">JXT1231212</a></th>
                                        <td>Natalie Harper</td>
                                        <td>Chandler Macleod</td>
                                        <td>Rick Mare</td>
                                        <td>Responsive Jobboard</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <hr/>
                        </div>
                    </div>
                    <div class="row">

                            <div class="col-sm-6">
                                <div class="form-inline selectProject">
                                <div class="form-group">
                                    <label for="">Select Project</label>
                                    <select class="form-control" name="" id="">
                                        <option value="">Responsive Jobboard</option>
                                        <option value="">Pintros</option>
                                        <option value="">Blog</option>
                                    </select>
                                </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-success pull-right">Go to Project</button>
                            </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <hr/>
                        </div>
                    </div>


                <div class="row">
                    <div class="col-sm-6">
                        <h2>Your tasks</h2>
                    <div class="table-responsive yourTasks">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Task name</th>
                                <th>Due Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td><a href="">Complete sitemap</a></td>
                                <td>12/12/2015</td>
                                <td>In Progress</td>
                            </tr>
                            <tr>
                                <td><a href="">Complete sitemap</a></td>
                                <td>12/12/2015</td>
                                <td>In Progress</td>
                            </tr>
                            <tr>
                                <td><a href="">Complete sitemap</a></td>
                                <td>12/12/2015</td>
                                <td>In Progress</td>
                            </tr>
                            <tr>
                                <td><a href="">Complete sitemap</a></td>
                                <td>12/12/2015</td>
                                <td>In Progress</td>
                            </tr>
                            <tr>
                                <td><a href="">Complete sitemap</a></td>
                                <td>12/12/2015</td>
                                <td>In Progress</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    </div>
                    <div class="col-sm-6">
                        <h2>JXT tasks</h2>
                        <div class="table-responsive jxtTasks">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Task name</th>
                                    <th>Due Date</th>
                                    <th>Status</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td><a href="">Complete sitemap</a></td>
                                    <td>12/12/2015</td>
                                    <td>In Progress</td>
                                </tr>
                                <tr>
                                    <td><a href="">Complete sitemap</a></td>
                                    <td>12/12/2015</td>
                                    <td>In Progress</td>
                                </tr>
                                <tr>
                                    <td><a href="">Complete sitemap</a></td>
                                    <td>12/12/2015</td>
                                    <td>In Progress</td>
                                </tr>
                                <tr>
                                    <td><a href="">Complete sitemap</a></td>
                                    <td>12/12/2015</td>
                                    <td>In Progress</td>
                                </tr>
                                <tr>
                                    <td><a href="">Complete sitemap</a></td>
                                    <td>12/12/2015</td>
                                    <td>In Progress</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                    <div class="col-sm-12">
                        <button class="btn btn-success">View all tasks</button>
                    </div>
                </div>


    </div>
</div>
</div>
</div>
</div>

<?php include 'includes/modals.php'; ?>

<?php include 'includes/footer.php'; ?>


</body>
</html>