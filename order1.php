<?php include 'includes/header.php'; ?>

<div class="container-fluid">
    <div class="row height-ref">

        <?php include 'includes/sidemenu.php'; ?>



        <div class="col-sm-10 order-section">
            <div class="row">
                <div class="col-sm-12">
                    <h1><span class="title-sub">Order No: </span> #JXTRef001942 <span class="title-sub">Company:</span>
                        JXT <span class="title-sub">BDM:</span> Neil Kearney </h1>

                    <hr>

                    <h2>Proposal Introduction</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ligula orci, pulvinar in
                        accumsan non, imperdiet eget diam. Donec quis pharetra ipsum. Ut efficitur massa in metus
                        hendrerit, ac malesuada enim feugiat. Vestibulum non magna in sem tincidunt hendrerit. Proin
                        vulputate mattis nisl, sed sodales neque dictum non. Proin nec arcu eget orci tincidunt eleifend
                        sed consequat ligula. Quisque eget bibendum elit, non dignissim urna.
                    </p>

                    <hr>

                    <p>Please follow the simple steps below to complete your order.</p>

                    <ul class="nav nav-pills nav-wizard">
                        <li class="active"><a href="order1.php">1. Your existing order</a>

                            <div class="nav-arrow"></div>
                        </li>
                        <li>
                            <div class="nav-wedge"></div>
                            <a href="order2.php">2. More Products from JXT</a>

                            <div class="nav-arrow"></div>
                        </li>
                        <li>
                            <div class="nav-wedge"></div>
                            <a href="order3.php">3. Confirmation of Order</a></li>
                    </ul>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <span class="glyphicon glyphicon-ok"></span>

                            <h3>Professional Services</h3>
                            <a>
                                <button class="btn btn-success pull-right showDetails">View Details</button>
                            </a>
                        </div>
                    </div>

                    <div class="product-detail">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div role="tabpanel">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home"
                                                                                  role="tab" data-toggle="tab">Description</a>
                                        </li>
                                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                                                   data-toggle="tab">Deliverables</a></li>
                                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab"
                                                                   data-toggle="tab">Terms and Conditions</a></li>
                                        <li role="presentation"><a href="#contract" aria-controls="contract" role="tab"
                                                                   data-toggle="tab">Length of Contract</a></li>
                                        <li role="presentation"><a href="#price" aria-controls="price" role="tab"
                                                                   data-toggle="tab">Price</a></li>
                                        <li role="presentation"><a href="#comments" aria-controls="comments" role="tab"
                                                                   data-toggle="tab">Wishlist/Comments</a></li>
                                    </ul>


                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="home">
                                            <p>JXT will provide you with a responsive company website alongside the
                                                integration of its unique proprietary job board technology. Your new
                                                website will provide visitors with a contemporary look and feel and the
                                                most up-to-date technology on the market.</p>

                                            <p>We use one of our cool designs to create your website, SEO compatibility,
                                                functional requirements, aesthetics and usability; enhancing the user
                                                experience.</p>

                                            <p>JXT’s job board and careers portal will also provide you with the ability
                                                to display all your jobs and careers related information on your own
                                                URL; including easy to navigate Job Search, Job Alerts and the ability
                                                to share Jobs on Social Media platforms.</p>

                                            <p>The talent acquisition environment is changing and companies who are at
                                                the cutting edge of technology will benefit greatly. Does your current
                                                website reflect your brand, business ambitions and culture?</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                        <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                        <div role="tabpanel" class="tab-pane" id="contract">...</div>
                                        <div role="tabpanel" class="tab-pane" id="price">...</div>
                                        <div role="tabpanel" class="tab-pane" id="comments">...</div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-body">
                            <span class="glyphicon glyphicon-ok"></span>

                            <h3>Professional Services</h3>
                            <a href="#">
                                <button class="btn btn-success pull-right">View Details</button>
                            </a>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <span class="glyphicon glyphicon-ok"></span>

                            <h3>Professional Services</h3>
                            <a href="#">
                                <button class="btn btn-success pull-right">View Details</button>
                            </a>
                        </div>
                    </div>

                    <a href="order2.php">
                        <button type="button" class="btn btn-success pull-right" style="margin-left:10px;">Next <span
                                class="glyphicon glyphicon-chevron-right"></span></button>
                    </a>
                    <a href="#">
                        <button type="button" class="btn btn-info pull-right"><span
                                class="glyphicon glyphicon-print"></span> Print your order
                        </button>
                    </a>


                </div>
            </div>
            <br/>


        </div>
    </div>
</div>
</div>

<?php include 'includes/modals.php'; ?>

<?php include 'includes/footer.php'; ?>


</body>
</html>